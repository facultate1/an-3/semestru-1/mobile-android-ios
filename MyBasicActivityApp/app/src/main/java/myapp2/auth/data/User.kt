package myapp2.auth.data

data class User(
    val email: String,
    val password: String
)
