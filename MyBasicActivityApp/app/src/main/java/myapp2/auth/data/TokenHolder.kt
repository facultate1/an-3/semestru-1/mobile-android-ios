package myapp2.auth.data

data class TokenHolder(
    val token: String
)
